{
  lib,
  config,
  ...
}: {
  options = {
    test_keymap = lib.mkOption {
      type = lib.types.attrs;
    };

    other_way_round = lib.mkOption {
      type = lib.types.anything;
    };
  };

  config = {
    test_keymap = {
      normal = {
        "<leader>a" = "doAction";
        "<leader>b" = "doAction";
      };
    };

    other_way_round = {
      # {
      #   doAction: ["a", "b"]
      # }
      normal =
        lib.lists.groupBy' (o: n: o ++ [n.mapping]) [] (val: val.action)
        (lib.mapAttrsToList (mapping: action: {
            inherit mapping;
            inherit action;
          })
          config.test_keymap.normal);
    };

    vim.nnoremap = let
      mapActions = action: command:
        lib.listToAttrs
        (map (mapping: lib.nameValuePair mapping command) config.other_way_round.normal.${action});

      mapping =
        mapActions "doAction" ":lua doIt();";
    in
      lib.traceSeq mapping mapping;
  };
}
