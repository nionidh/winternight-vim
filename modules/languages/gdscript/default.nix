{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with builtins; let
  cfg = config.vim.languages.gdscript;

  defaultServer = "godot";
  servers = {
    godot = {
      lspConfig = ''
        lspconfig.gdscript.setup {
          capabilities = capabilities;
          on_attach = default_on_attach;
        }
      '';
    };
  };
in {
  options.vim.languages.gdscript = {
    enable = mkEnableOption "GDScript language support";

    treesitter = {
      enable = mkOption {
        description = "Enable GDScript treesitter";
        type = types.bool;
        default = config.vim.languages.enableTreesitter;
      };
      package = nvim.types.mkGrammarOption pkgs "gdscript";
    };

    lsp = {
      enable = mkOption {
        description = "Enable GDScript LSP support";
        type = types.bool;
        default = config.vim.languages.enableLSP;
      };
      server = mkOption {
        description = "GDScript LSP server to use";
        type = with types; enum (attrNames servers);
        default = defaultServer;
      };
      package = mkOption {
        description = "GDScript LSP server package";
        type = types.package;
        default = servers.${cfg.lsp.server}.package;
      };
    };
  };

  config = mkIf cfg.enable (mkMerge [
    (mkIf cfg.treesitter.enable {
      vim.treesitter.enable = true;
      vim.treesitter.grammars = [cfg.treesitter.package];
    })

    (mkIf cfg.lsp.enable {
      vim.lsp.lspconfig.enable = true;
      vim.lsp.lspconfig.sources.gdscript = servers.${cfg.lsp.server}.lspConfig;
    })
  ]);
}
