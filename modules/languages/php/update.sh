#!/usr/bin/env bash

PACKAGES_FILE="$(readlink -f $(dirname $0))/packages.json"
rm ${PACKAGES_FILE}

jq -c '(.packages + ."packages-dev")[] | {name: .name, version: .version, url: .dist.url}' phpactor_composer.lock | while read dep;
do
    
    if [[ "${NEXTLINE}" != "" ]];
    then
        echo "${NEXTLINE}," >> ${PACKAGES_FILE}
    fi

    URL=$(echo $dep | jq -r '.url')
    NAME=$(echo $dep | jq -r '.name')
    VERSION=$(echo $dep | jq -r '.version')

    echo "${URL}" >&2

    SUM=$(nix-prefetch fetchzip --url "${URL}" --extension zip -q)
    # SUM="A"

    NEXTLINE="{\"name\": \"${NAME}\", \"url\": \"${URL}\", \"version\": \"${VERSION}\", \"sha256\": \"${SUM}\"}"
    echo ${NEXTLINE}
done | jq -n ". |= [inputs]" > ${PACKAGES_FILE}
