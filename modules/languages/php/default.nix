{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with builtins; let
  cfg = config.vim.languages.php;

  defaultServer = "intelephense";
  servers = {
    intelephense = {
      package = pkgs.nodePackages.intelephense;
      lspConfig = ''
        lspconfig.intelephense.setup {
          on_attach = default_on_attach;
          cmd = {"${cfg.lsp.package}/bin/intelephense", "--stdio"},
        }
      '';
    };
    phpactor = {
      package = pkgs.callPackage ./phpactor.nix {};
      lspConfig = ''
        lspconfig.phpactor.setup {
          on_attach = default_on_attach;
          cmd = {"${cfg.lsp.package}/bin/phpactor", "language-server"},
        }
      '';
    };
  };
in {
  options.vim.languages.php = {
    enable = mkEnableOption "Php language support";

    treesitter = {
      enable = mkOption {
        description = "Enable Php treesitter";
        type = types.bool;
        default = config.vim.languages.enableTreesitter;
      };
      package = nvim.types.mkGrammarOption pkgs "php";
    };

    lsp = {
      enable = mkOption {
        description = "Enable Php LSP support";
        type = types.bool;
        default = config.vim.languages.enableLSP;
      };
      server = mkOption {
        description = "Php LSP server to use";
        type = with types; enum (attrNames servers);
        default = defaultServer;
      };
      package = mkOption {
        description = "Php LSP server package";
        type = types.package;
        default = servers.${cfg.lsp.server}.package;
      };
    };
  };

  config = mkIf cfg.enable (mkMerge [
    (mkIf cfg.treesitter.enable {
      vim.treesitter.enable = true;
      vim.treesitter.grammars = [cfg.treesitter.package];
    })

    (mkIf cfg.lsp.enable {
      vim.lsp.lspconfig.enable = true;
      vim.lsp.lspconfig.sources.php-lsp = servers.${cfg.lsp.server}.lspConfig;
    })
  ]);
}
