{
  pkgs,
  lib,
  ...
}: let
  composerLock = builtins.fromJSON (builtins.readFile ./packages.json);

  vendoredPackages =
    map (
      package: {
        inherit package;
        drv = pkgs.fetchzip {
          name = package.name;
          url = package.url;
          sha256 = package.sha256;
          extension = "zip";
        };
      }
    )
    composerLock;

  copyVendored =
    map (package: let
      path = "$out/vendor/${package.package.name}";
    in ''
      mkdir -p ${builtins.dirOf path}
      cp -r ${lib.traceVal (toString package.drv)} ${lib.traceVal path}
    '')
    vendoredPackages;

  installedJson = pkgs.writeTextFile {
    name = "installed.json";
    text = builtins.readFile ./installed.json;
  };
  installedPhp = pkgs.writeTextFile {
    name = "installed.php";
    text = builtins.readFile ./installed.php;
  };
  installedVersions = pkgs.writeTextFile {
    name = "InstalledVersions.php";
    text = builtins.readFile ./InstalledVersions.php;
  };
in
  pkgs.stdenv.mkDerivation {
    pname = "phpactor";
    version = "2023-04-10";
    src =
      pkgs.fetchFromGitHub
      {
        owner = "phpactor";
        repo = "phpactor";
        rev = "1b92b62ccf35491bc8d378c991479a0d5e3c2c1d";
        sha256 = "nEerwOrXsghdLxG1iVK5pNgLkYFJWqmfL4HMRkjiYdI=";
      };

    unpackPhase = '''';
    buildPhase = ''
    '';
    installPhase = ''
      mkdir -p $out
      cp -R . $out

      cd $out
      mkdir -p $out/vendor
      ${lib.strings.concatStringsSep "\n" copyVendored}
      mkdir -p $out/vendor/composer
      cp ${installedJson} $out/vendor/composer/installed.json
      cp ${installedPhp} $out/vendor/composer/installed.php
      cp ${installedVersions} $out/vendor/composer/InstalledVersions.php
      composer dump-autoload
      # composer install --optimize-autoloader
    '';
    nativeBuildInputs = [pkgs.php82 pkgs.php82Packages.composer];
  }
