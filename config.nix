{lib, ...}: {
  imports = [./modules/languages/php ./modules/languages/gdscript];
  config = {
    vim = {
      #TODO nvim-lspconfig
      lsp = {
        formatOnSave = true;
        lspconfig.enable = true;
        trouble.enable = true;
        lspkind.enable = true;
        fidget.enable = true;
        nvimCodeActionMenu.enable = true;
      };
      #TODO nlsp-settings
      #TODO null-ls
      #TODO structlog
      #TODO popup.nvim
      #TODO telescope.nvim
      telescope = {
        enable = true;
      };

      #TODO telescope-fzf-native
      #TODO nvim-cmp
      #TODO friendly-snippets
      #TODO LuaSnip

      #TODO cmp-nvim-lsp
      #TODO cmp_luasnip
      #TODO cmp-buffer
      #TODO cmp-path
      autocomplete = {
        enable = true;
        #window = {
        #  completion = "cmp.config.window.bordered()";
        #};
      };

      #TODO nvim-autopairs
      autopairs = {
        enable = true;
      };
      #TODO nvim-treesitter
      #TODO nvim-ts-context-commentstring
      #nvim-tree
      filetree.nvimTreeLua = {
        enable = true;
        closeOnFileOpen = true;
        followBufferFile = true;
      };

      keymap = {
        normal = actions: {
          "<silent><leader>e" = actions.nvimTreeLua.findFileToggle;
          "<silent><leader>E" = actions.nvimTreeLua.toggle;
          "<C-.>" = actions.lsp.codeAction;
          "<C-p>" = actions.telescope.findFiles;
          "<C-Tab>" = actions.telescope.buffers;
          "<leader>st" = actions.telescope.liveGrep;
          "<leader>sr" = actions.telescope.lspReferences;
          "<leader>ls" = actions.telescope.lspWorkspaceSymbols;

          "<C-l>" = actions.executeViml ":wincmd l<CR>";
          "<C-h>" = actions.executeViml ":wincmd h<CR>";
          "<M-L>" = actions.nvimBufferline.cycleNext;
          "<M-H>" = actions.nvimBufferline.cyclePrev;
          "<silent><leader>bl" = actions.nvimBufferline.cycleNext;
          "<silent><leader>bh" = actions.nvimBufferline.cyclePrev;
          "<silent><leader>bc" = actions.executeViml ":BufferLinePickClose<CR>";
          "gd" = actions.lsp.gotoDefinition;
          "<C-k>" = actions.lsp.hover;
          "<space>c" = actions.executeViml ":bd<CR>";
          "<space>lr" = actions.lsp.rename;
          "<space>td" = actions.trouble.documentDiagnostics;
          "<space>tw" = actions.trouble.workspaceDiagnostics;
        };
        insert = actions: {
          "<C-p>" = actions.lsp.signatureHelp;
          "<C-d>" = actions.autocomplete.docsDown;
          "<C-f>" = actions.autocomplete.docsUp;
          "<C-Space>" = actions.autocomplete.complete;
          "<C-y>" = actions.autocomplete.disable;
          "<C-e>" = actions.autocomplete.abort;
          "<C-j>" = actions.autocomplete.next;
          "<C-k>" = actions.autocomplete.prev;
          "<CR>" = actions.autocomplete.confirm;
          "<Tab>" = actions.autocomplete.doTabAction;
          "<S-Tab>" = actions.autocomplete.doShiftTabAction;
        };
        command = actions: {
          "<C-d>" = actions.autocomplete.docsDown;
          "<C-f>" = actions.autocomplete.docsUp;
          "<C-Space>" = actions.autocomplete.complete;
          "<C-y>" = actions.autocomplete.disable;
          "<C-e>" = actions.autocomplete.close;
          "<CR>" = actions.autocomplete.confirm;
        };
        select = actions: {
          "<C-y>" = actions.autocomplete.disable;
          "<CR>" = actions.autocomplete.confirm;
          "<Tab>" = actions.autocomplete.doTabAction;
          "<S-Tab>" = actions.autocomplete.doShiftTabAction;
        };
      };

      #TODO gitsigns
      #TODO Comment
      #TODO project.nvim
      #nvim-web-devicons
      visuals.nvimWebDevicons.enable = true;
      #TODO lualine
      statusline.lualine = {
        enable = true;
      };
      #TODO nvim-navic
      #bufferline
      tabline.nvimBufferline = {
        enable = true;
      };
      #TODO nvim-dap
      #TODO nvim-dap-ui
      #TODO alpha-nvim
      #TODO toggleterm.nvim
      #TODO schemastore.nvim
      #TODO vim-illuminate

      #indnet-blankline
      visuals.indentBlankline.enable = true;

      languages = {
        enableLSP = true;
        enableTreesitter = true;
        enableFormat = true;
        enableExtraDiagnostics = false;

        rust = {
          enable = true;
          lsp = {
            server = "rust-analyzer";
          };
        };
        nix = {
          enable = true;
        };
        gdscript = {
          enable = true;
        };
        php = {
          enable = true;
          lsp = {
            server = "phpactor";
          };
        };
        html = {
          enable = true;
        };
      };

      #dracula
      theme.enable = true;
      theme.name = "dracula";

      mapLeaderSpace = true;

      visuals.enable = true;
      visuals.cursorWordline.enable = true;

      keys = {
        enable = true;
        whichKey.enable = true;
      };
    };
  };
}
