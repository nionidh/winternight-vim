{winternight-vim}: {lib, ...}: {
  options = {
    winternight-vim = {
      enable = lib.mkOption {
        default = false;
        type = lib.types.bool;
      };
    };
  };
  config = {
    environment.systemPackages = [
      winternight-vim
    ];
  };
}
