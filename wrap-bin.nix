# Configuration is passed into here using the overrides pattern
{
  pkgs,
  # stdenv,
  runCommand,
  writeShellApplication,
  writeTextFile,
  writeShellScriptBin,
  symlinkJoin,
  ...
}: customNeovim: let
  executeFile =
    writeShellScriptBin
    "wim"
    ''
      ${toString customNeovim}/bin/nvim
    '';
in
  symlinkJoin {
    name = "winternight-vim";
    paths = [executeFile];
  }
