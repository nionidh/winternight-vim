# This will expose nixosModuels that include winternight in packages and then
# pass the configuration to them via the overrides pattern
{
  description = "Nionidh's opinionated LunarVim inspired vim flake";

  inputs = {
    nixpkgs = {
      url = "nixpkgs/nixos-22.11";
    };
    neovim-flake = {
      url = "github:MonaMayrhofer/neovim-flake";
      flake = true;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    nixpkgs,
    neovim-flake,
    self,
    ...
  } @ inputs: let
    system = "x86_64-linux";
    wimBin = package: "${package}/bin/wim";
    # pkgs = nixpkgs.legacyPackages.${system};

    lib = import nixpkgs {
      inherit system;
      config.allowUnfree = true;
    };
    pkgs = lib;
    wrapBin = lib.callPackage ./wrap-bin.nix {};

    # configModule.config.vim = lib.callPackage ./config.nix {inherit inputs;};
    configModule = import ./config.nix;

    additionalRawPlugins =
      inputs.neovim-flake.lib.nvim.plugins.inputsToRaw inputs
      [
      ];

    customNeovim = neovim-flake.lib.neovimConfiguration {
      modules = [{config.build.rawPlugins = additionalRawPlugins;} configModule];
      inherit pkgs;
    };
    winternight-vim = wrapBin customNeovim;
  in {
    overlay = final: prev: {
      winternight-wim = winternight-vim;
    };
    packages.${system} = {
      winternight-vim = winternight-vim;
      default = winternight-vim;
    };

    apps.${system}.default = {
      type = "app";
      program = wimBin self.packages.x86_64-linux.default;
    };

    nixosModules = {
      winternight-vim = import ./module.nix {
        inherit winternight-vim;
      };
    };
  };
}
