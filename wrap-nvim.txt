# Docs: https://github.com/NixOS/nixpkgs/blob/22.11/pkgs/build-support/trivial-builders.nix
{
  runCommand,
  writeTextFile,
  writeTextDir,
  stdenv,
  lib,
  pkgs,
  ...
}: let
in
# runCommand "winternight-nvim-wrapper" {
#   #https://nixos.org/manual/nix/stable/language/advanced-attributes.html
#   #TODO Local and disable substitutes... no binary cache for these needed
#   wrapperScript = "AGAAGGAA $out";
#   passAsFile = ["wrapperScript"];
#   #https://nixos.org/manual/nixpkgs/stable/#sec-using-stdenv
#   configs = stdenv.mkDerivation {
#     name = "init.lua";
#     text = ''
#       Init lua
#     '';
#   };
# }
# ''
#   echo "$0"
#   while [[ $# -gt 0 ]] ; do
#     echo "$1"
#     shift
#   done
#   echo "WSRP": $wrapperScriptPath
#   cat $wrapperScriptPath
#   echo ""
#   echo "Configs": $configs
#   echo "aa:" $aa
#   echo "out:" $out
#   exit 10
# ''

